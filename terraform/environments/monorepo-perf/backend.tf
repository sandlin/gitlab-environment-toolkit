# Store state in the project.
terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/28595566/terraform/state/test"
    lock_address = "https://gitlab.com/api/v4/projects/28595566/terraform/state/test/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/28595566/terraform/state/test/lock"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
  }
}
