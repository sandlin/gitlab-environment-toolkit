# https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_provision.md#configure-variables-variablestf
prefix = "monorepo-perf"
service_account_id = "gitlab-sa-id"
project = "jsandlin-c9fe7132"
region = "us-west1"
zone = "us-west1-a"
external_ip = "34.145.52.149"