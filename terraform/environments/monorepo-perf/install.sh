#!/bin/bash -x
if [[ ! -f "./monorepo_perf_token.tfvars" ]]
then
  echo "Cannot find .tfvars in " + pwd
  return 1
fi
# DEBUG OPTION
#export TF_LOG=TRACE
IP=`curl ifconfig.me`
PLAN_FILE=myplan.tfplan
PLAN_JSON=myplan.json
rm $PLAN_FILE $PLAN_JSON
# Create the EKS cluster
terraform init -backend-config=./.creds
rm -f $PLAN_FILE $PLAN_JSON
terraform plan -var-file="./.creds" -out $PLAN_FILE
terraform show -json $PLAN_FILE > $PLAN_JSON
if [[ "$1" == "create" ]]
then
  terraform apply -var-file="./.creds" --auto-approve
elif [[ "$1" == "destroy" ]]
then
  terraform destroy -var-file="./.creds" --auto-approve
fi