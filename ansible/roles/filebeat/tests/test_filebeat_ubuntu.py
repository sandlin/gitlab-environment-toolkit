import testinfra
import uuid
import time
import requests

def test_filebeat_package(host):
    assert host.package("filebeat").is_installed

def test_filebeat_command(host):
    assert host.run("filebeat --version").rc == 0

#def test_filebeat_files(host):
#    assert host.file('/etc/filebeat/filebeat.yml').exists

#def test_filebeat_config(host):
#    assert host.run("sudo filebeat test config").rc == 0

def test_filebeat_service(host):
    assert host.service('filebeat').is_enabled

def test_filebeat_functioning(host):
    host_vars = host.ansible.get_variables()
    assert 'inventory_hostname' in host_vars
    hostname = host_vars['inventory_hostname']
    #
    # assert 'app' in host_vars
    # app_name = host_vars['app']
    #
    # assert 'urls' in host_vars
    # assert 'elasticsearch' in host_vars['urls']
    # elasticsearch_url = host_vars['urls']['elasticsearch']
    # assert elasticsearch_url.startswith("http")
    #
    # # Create unique string
    uuid_string = "FILEBEAT VERIFICATION UUID: {}".format(uuid.uuid1())
    print('\n{}'.format(uuid_string))

    # Write string to syslog
    cmd = host.run("sudo logger %s", str(uuid_string))
    assert cmd.rc == 0

    # Wait for filebeat to read syslog entry and send to elasticsearch
    time.sleep(20)

    # Read string back from elasticsearch
    # query = '{}/{}-*/_search?q="{}"'.format(elasticsearch_url, app_name, uuid_string)
    # print('\nQUERY: {}'.format(query))
    # response = requests.get(query)
    # print('\nRESPONSE: {}'.format(response))
    # assert response.status_code == 200
    # response_json = response.json()

    # Verify that string was found
    #assert response_json['hits']['total'] > 0
