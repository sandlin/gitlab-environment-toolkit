import testinfra
import uuid
import time
import requests

def test_filebeat_package(host):
    assert host.run("/usr/local/bin/brew list filebeat").rc == 0

def test_filebeat_command(host):
    assert host.run("/usr/local/bin/filebeat --version").rc == 0

#def test_filebeat_files(host):
#    assert host.file('/usr/local/var/homebrew/linked/filebeat/filebeat.yml').exists

#def test_filebeat_config(host):
#    assert host.run("sudo /usr/local/bin/filebeat test config -c /usr/local/var/homebrew/linked/filebeat/filebeat.yml").rc == 0

def test_filebeat_service(host):
    results = host.run("/usr/local/bin/brew services list | grep filebeat | grep started | wc -l").stdout
    assert int(results) == 1
