filebeat
=========

Installs and configures the Filebeat agent on a machine as part of the ELK stack.
Note that filebeat modules are disabled for now.

Requirements
------------

* Testinfra installed through pip (used for testing deploy)

Role Variables
--------------

* `monitoring.filebeat.modules`:[required, group_vars]: filebeat modules. DISABLED FOR NOW.

Dependencies
------------

None

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - { role: filebeat }
```

Testing
-------

### Molecule

This role supports molecule testing.

The test files used for molecule testing can be found at:

`PATH_TO_ROLES/filebeat/molecule/default/tests/test_default.py`

##### Requirements

* `Docker-ce` or `Docker-for-mac` must be installed on your local machine.
* Molecule installed through pip
* Ansible-vault password file created in `$HOME/.python/vaultpass`

##### Running Molecule tests

From the root of this role (`/PATH_TO_ROLES/filebeat/`) run molecule command -> `molecule test`

```bash
# go to root of role
cd PATH_TO_ROLES/filebeat

# Run Molecule tests
molecule test
```

### Testinfra

Testinfra is configured to run at the end of this role automatically to test that the role got setup properly.

The test files used for testinfra testing can be found at:

`PATH_TO_ROLES/filebeat/tests/test_filebeat_macos.py`
or
`PATH_TO_ROLES/filebeat/tests/test_filebeat_ubuntu.py`

##### Requirements

* Testinfra installed through pip

License
-------

BSD

Author Information
------------------


